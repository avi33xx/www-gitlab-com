---
title: 5 key organizational models for DevOps teams
author: Johanna Ambrosio
author_twitter: gitlab
categories: insights
tags: DevOps, collaboration, growth
description: DevOps teams can be organized in multiple ways. Identify the one
  that fits your organization.
image_title: /images/blogimages/2020-11-19-integration-management-header.jpg
twitter_text: DevOps teams can be organized in multiple ways. Identify the one
  that fits your organization.
---
If you’re just getting started with DevOps, there are several team organizational models to consider.

A few key points to keep in mind as you design your team structure:

- The organizational model you start with should change as you add more people, [different DevOps roles](/blog/2022/01/25/how-to-build-out-your-devops-team/), and more projects. Expect to keep iterating as you go.

- The ultimate goal of DevOps is to spread the message, tools, and processes throughout the company so that, eventually, everyone is working “the DevOps way.” At some point, if your approach is successful, DevOps as a separate group will disappear.

- The model you begin with should depend on how many projects or products you’re working on, the size of your teams, and the size of your company. 

- Keep your team size small, with three to eight people max. Some experts say up to 12 is OK, but that’s a bit large for the [“two-pizza” rule](https://landing.directorpoint.com/blog/amazon-two-pizza-rule/). 

Here are five DevOps organizational models to consider as you get going, according to Matthew Skelton and Manuel Pais, experts who wrote a book called Team Topologies about this topic and then updated the book with a [related microsite](https://web.devopstopologies.com). Their work is a must-read for anyone who’s trying to figure out which DevOps structure is best for their company.

**1. Dev and ops co-exist, with a “DevOps” group in between**

This can be a good interim strategy until you can build out a full DevOps program. The DevOps team translates between the two groups, which pretty much stay in place as they currently are, and DevOps facilitates all work on a project. 

Just don’t keep this structure in place too long. You don’t want to reinforce the separate silos as they currently exist for any longer than absolutely necessary.

**2. Dev and ops groups remain separate organizationally but on equal footing**
 
This is also a reasonable place to start: Everyone collaborates but can specialize where needed. Common tools will go a long way to helping facilitate good communication. In this model, several dev teams can be working on different products or services. 

Make sure teams communicate regularly. Invite a rep from each camp to the other’s meetings, for instance. And appoint a liaison to the rest of the company to make sure executives and line-of-business leaders know how DevOps is going, and so dev and ops can be part of conversations about the top corporate priorities.

**3. Create one team, maybe “no ops”?**

In this model, a single team has shared goals with no separate functions. The reason it’s called [“no ops”](https://searchitoperations.techtarget.com/definition/NoOps) is because ops is so automated it’s like it doesn’t actually exist. 

This level of automation is so “aspirational” that many experts express caution about this approach. To eliminate any hands-on tasks, teams would need extensive machine learning and artificial intelligence solutions, and a flat, streamlined organization that prioritizes communication and workflow. TL;DR: [NoOps may not ever be a reality](https://www.cio.com/article/220351/what-is-noops-the-quest-for-fully-automated-it-operations.html).

However, don’t use this as an excuse to do away with the ops team. You are going to need those folks. Devs can’t do it all.

**4. Ops as infrastructure consultants**

This model works best for companies with a traditional IT group that has multiple projects and includes ops pros. It’s also good for those using a lot of cloud services or expecting to do so. 

Here, ops acts as an internal consultant to create scalable web services and cloud compute capacity, a sort of mini-web services provider. In our [2021 Global DevSecOps Survey](/developer-survey/), a plurality of ops pros told us this is _exactly_ how their jobs are evolving — out of wrestling toolchains and into ownership of the team’s cloud computing efforts. Dev teams continue to do their work, with DevOps specialists within the dev group responsible for metrics, monitoring, and communicating with the ops team.

**5. DevOps-as-a-service**

You may decide your organization just doesn’t have the internal expertise or resources to create your own DevOps initiative, so you should hire an outside firm or consultancy to get started. This [DevOps-as-a-service (DaaS) model](https://medium.com/swlh/pros-and-cons-of-devops-as-a-service-a40b8796533c) is especially helpful for small companies with limited in-house IT skills.

Using DaaS in the short term offers another advantage: the opportunity to learn from your outsourcer how to eventually create your own internal DevOps team.

Make sure you understand the outsourcer’s security landscape and your own responsibilities in this area, as you would with any outside firm. The difference here is that the team, processes, and software the outsourcer plans to use will be deeply embedded in your company’s infrastructure — it’s not something you can easily switch from. Also ensure that the outsourcer’s tools will work with what you already have in-house.

Finally, keep a keen eye on costs and understand how the outsourcer will charge for its services.

**Other organizational schemes include:**

A two-tier model, with a business systems team responsible for the end-to-end product cycle and platform teams that manage the underlying hardware, software, and other infrastructure. 
DevOps and SRE groups are separate, with DevOps part of the dev team and Site Reliability Engineers part of ops. This model requires a mature operations and development culture. 

Whichever organization model you choose, remember the idea of DevOps is to break down silos, not create new ones. Constantly reevaluate what’s working, what’s not, and how to deliver most effectively what your customers need.

_Johanna Ambrosio is a technology writer in the greater Boston area._
